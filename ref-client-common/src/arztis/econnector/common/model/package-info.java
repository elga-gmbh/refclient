/**
 * Provides the classes which are needed to handle SOAP/REST requests and responses like requesting card data for ELGA reference client. 
 */
package arztis.econnector.common.model;