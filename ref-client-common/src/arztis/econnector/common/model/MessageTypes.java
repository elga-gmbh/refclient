/**
 * 
 * Contributors:
 *   ET-Innovations GmbH - initial API and implementation
 * 
 */
package arztis.econnector.common.model;

/**
 * List of different message types. At the moment there are two different types.
 * One for error messages and another for successful messages.
 *
 * @author Anna Jungwirth
 *
 */
public enum MessageTypes {
	
	/** type for error messages. */
	ERROR,

	/** type for successful messages. */
	SUCCESS
}
