/**
 * 
 * Contributors:
 *   ET-Innovations GmbH - initial API and implementation
 * 
 */
package arztis.econnector.common.model;

/**
 * List of different gender. At the moment there are three different types. One
 * for males and another for females. And a type unknown for all other.
 *
 * @author Anna Jungwirth
 *
 */
public enum SexCode {

	/** The male. */
	MALE,
	/** The female. */
	FEMALE,
	/** The unknown. */
	UNKNOWN
}
