/**
 * Provides functionalities for common purposes like authentication.
 */
package arztis.econnector.common;