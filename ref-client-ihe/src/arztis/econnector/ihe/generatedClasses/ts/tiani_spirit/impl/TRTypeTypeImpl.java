/*
 * XML Type:  TRTypeType
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.TRTypeType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML TRTypeType(@urn:tiani-spirit:ts).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.TRTypeType.
 */
public class TRTypeTypeImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.TRTypeType
{
    private static final long serialVersionUID = 1L;
    
    public TRTypeTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected TRTypeTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
