/*
 * XML Type:  ClaimType_Name
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeName
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML ClaimType_Name(@urn:tiani-spirit:ts).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeName.
 */
public class ClaimTypeNameImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.ClaimTypeName
{
    private static final long serialVersionUID = 1L;
    
    public ClaimTypeNameImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ClaimTypeNameImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
