/*
 * XML Type:  IndividualResponsePolicyType
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.IndividualResponsePolicyType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML IndividualResponsePolicyType(@urn:tiani-spirit:ts).
 *
 * This is a complex type.
 */
public class IndividualResponsePolicyTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.IndividualResponsePolicyType
{
    private static final long serialVersionUID = 1L;
    
    public IndividualResponsePolicyTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
