/*
 * XML Type:  IdentMethodType
 * Namespace: urn:tiani-spirit:ts
 * Java type: arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.IdentMethodType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.impl;
/**
 * An XML IdentMethodType(@urn:tiani-spirit:ts).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.IdentMethodType.
 */
public class IdentMethodTypeImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.ts.tiani_spirit.IdentMethodType
{
    private static final long serialVersionUID = 1L;
    
    public IdentMethodTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected IdentMethodTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
