/*
 * XML Type:  referenceURI
 * Namespace: urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0
 * Java type: arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.ReferenceURI
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.impl;
/**
 * An XML referenceURI(@urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.ReferenceURI.
 */
public class ReferenceURIImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.ReferenceURI
{
    private static final long serialVersionUID = 1L;
    
    public ReferenceURIImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ReferenceURIImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
