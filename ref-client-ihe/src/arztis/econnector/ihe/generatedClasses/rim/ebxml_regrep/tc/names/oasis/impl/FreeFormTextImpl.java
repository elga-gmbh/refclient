/*
 * XML Type:  FreeFormText
 * Namespace: urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0
 * Java type: arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.FreeFormText
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.impl;
/**
 * An XML FreeFormText(@urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.FreeFormText.
 */
public class FreeFormTextImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements arztis.econnector.ihe.generatedClasses.rim.ebxml_regrep.tc.names.oasis.FreeFormText
{
    private static final long serialVersionUID = 1L;
    
    public FreeFormTextImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected FreeFormTextImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
