/*
 * XML Type:  AssertionType
 * Namespace: urn:oasis:names:tc:SAML:2.0:assertion
 * Java type: arztis.econnector.ihe.generatedClasses.assertion._0._2.saml.tc.names.oasis.AssertionType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.assertion._0._2.saml.tc.names.oasis.impl;
/**
 * An XML AssertionType(@urn:oasis:names:tc:SAML:2.0:assertion).
 *
 * This is a complex type.
 */
public class AssertionTypeImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements arztis.econnector.ihe.generatedClasses.assertion._0._2.saml.tc.names.oasis.AssertionType
{
    private static final long serialVersionUID = 1L;
    
    public AssertionTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
