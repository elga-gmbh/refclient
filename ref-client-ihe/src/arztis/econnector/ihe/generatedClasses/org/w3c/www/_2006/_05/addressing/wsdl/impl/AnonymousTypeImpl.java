/*
 * XML Type:  AnonymousType
 * Namespace: http://www.w3.org/2006/05/addressing/wsdl
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.AnonymousType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.impl;
/**
 * An XML AnonymousType(@http://www.w3.org/2006/05/addressing/wsdl).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.AnonymousType.
 */
public class AnonymousTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.AnonymousType
{
    private static final long serialVersionUID = 1L;
    
    public AnonymousTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected AnonymousTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
