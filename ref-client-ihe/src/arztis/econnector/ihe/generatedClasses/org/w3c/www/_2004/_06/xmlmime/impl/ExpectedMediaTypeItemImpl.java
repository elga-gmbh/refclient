/*
 * XML Type:  expectedMediaTypeItem
 * Namespace: http://www.w3.org/2004/06/xmlmime
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www._2004._06.xmlmime.ExpectedMediaTypeItem
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www._2004._06.xmlmime.impl;
/**
 * An XML expectedMediaTypeItem(@http://www.w3.org/2004/06/xmlmime).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.org.w3c.www._2004._06.xmlmime.ExpectedMediaTypeItem.
 */
public class ExpectedMediaTypeItemImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements arztis.econnector.ihe.generatedClasses.org.w3c.www._2004._06.xmlmime.ExpectedMediaTypeItem
{
    private static final long serialVersionUID = 1L;
    
    public ExpectedMediaTypeItemImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ExpectedMediaTypeItemImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
