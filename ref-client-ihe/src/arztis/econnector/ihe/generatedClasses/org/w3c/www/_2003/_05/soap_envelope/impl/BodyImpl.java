/*
 * XML Type:  Body
 * Namespace: http://www.w3.org/2003/05/soap-envelope
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.Body
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.impl;
/**
 * An XML Body(@http://www.w3.org/2003/05/soap-envelope).
 *
 * This is a complex type.
 */
public class BodyImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.Body
{
    private static final long serialVersionUID = 1L;
    
    public BodyImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
