/*
 * XML Type:  OperatorContentType
 * Namespace: http://www.w3.org/ns/ws-policy
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy;


/**
 * An XML OperatorContentType(@http://www.w3.org/ns/ws-policy).
 *
 * This is a complex type.
 */
public interface OperatorContentType extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(OperatorContentType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sA176CE12641784571DF8C98A0DCB19FE").resolveHandle("operatorcontenttype6f47type");
    
    /**
     * Gets array of all "Policy" elements
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyDocument.Policy[] getPolicyArray();
    
    /**
     * Gets ith "Policy" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyDocument.Policy getPolicyArray(int i);
    
    /**
     * Returns number of "Policy" element
     */
    int sizeOfPolicyArray();
    
    /**
     * Sets array of all "Policy" element
     */
    void setPolicyArray(arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyDocument.Policy[] policyArray);
    
    /**
     * Sets ith "Policy" element
     */
    void setPolicyArray(int i, arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyDocument.Policy policy);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "Policy" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyDocument.Policy insertNewPolicy(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "Policy" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyDocument.Policy addNewPolicy();
    
    /**
     * Removes the ith "Policy" element
     */
    void removePolicy(int i);
    
    /**
     * Gets array of all "All" elements
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType[] getAllArray();
    
    /**
     * Gets ith "All" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType getAllArray(int i);
    
    /**
     * Returns number of "All" element
     */
    int sizeOfAllArray();
    
    /**
     * Sets array of all "All" element
     */
    void setAllArray(arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType[] allArray);
    
    /**
     * Sets ith "All" element
     */
    void setAllArray(int i, arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType all);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "All" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType insertNewAll(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "All" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType addNewAll();
    
    /**
     * Removes the ith "All" element
     */
    void removeAll(int i);
    
    /**
     * Gets array of all "ExactlyOne" elements
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType[] getExactlyOneArray();
    
    /**
     * Gets ith "ExactlyOne" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType getExactlyOneArray(int i);
    
    /**
     * Returns number of "ExactlyOne" element
     */
    int sizeOfExactlyOneArray();
    
    /**
     * Sets array of all "ExactlyOne" element
     */
    void setExactlyOneArray(arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType[] exactlyOneArray);
    
    /**
     * Sets ith "ExactlyOne" element
     */
    void setExactlyOneArray(int i, arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType exactlyOne);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ExactlyOne" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType insertNewExactlyOne(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ExactlyOne" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType addNewExactlyOne();
    
    /**
     * Removes the ith "ExactlyOne" element
     */
    void removeExactlyOne(int i);
    
    /**
     * Gets array of all "PolicyReference" elements
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyReferenceDocument.PolicyReference[] getPolicyReferenceArray();
    
    /**
     * Gets ith "PolicyReference" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyReferenceDocument.PolicyReference getPolicyReferenceArray(int i);
    
    /**
     * Returns number of "PolicyReference" element
     */
    int sizeOfPolicyReferenceArray();
    
    /**
     * Sets array of all "PolicyReference" element
     */
    void setPolicyReferenceArray(arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyReferenceDocument.PolicyReference[] policyReferenceArray);
    
    /**
     * Sets ith "PolicyReference" element
     */
    void setPolicyReferenceArray(int i, arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyReferenceDocument.PolicyReference policyReference);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "PolicyReference" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyReferenceDocument.PolicyReference insertNewPolicyReference(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "PolicyReference" element
     */
    arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.PolicyReferenceDocument.PolicyReference addNewPolicyReference();
    
    /**
     * Removes the ith "PolicyReference" element
     */
    void removePolicyReference(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType newInstance() {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.org.w3c.www.ns.ws_policy.OperatorContentType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
