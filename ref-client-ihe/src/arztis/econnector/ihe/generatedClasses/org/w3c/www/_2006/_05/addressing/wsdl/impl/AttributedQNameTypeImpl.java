/*
 * XML Type:  AttributedQNameType
 * Namespace: http://www.w3.org/2006/05/addressing/wsdl
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.AttributedQNameType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.impl;
/**
 * An XML AttributedQNameType(@http://www.w3.org/2006/05/addressing/wsdl).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.AttributedQNameType.
 */
public class AttributedQNameTypeImpl extends org.apache.xmlbeans.impl.values.JavaQNameHolderEx implements arztis.econnector.ihe.generatedClasses.org.w3c.www._2006._05.addressing.wsdl.AttributedQNameType
{
    private static final long serialVersionUID = 1L;
    
    public AttributedQNameTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, true);
    }
    
    protected AttributedQNameTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
    
    
}
