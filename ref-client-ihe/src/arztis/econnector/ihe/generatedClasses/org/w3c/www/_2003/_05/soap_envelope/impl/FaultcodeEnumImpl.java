/*
 * XML Type:  faultcodeEnum
 * Namespace: http://www.w3.org/2003/05/soap-envelope
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.FaultcodeEnum
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.impl;
/**
 * An XML faultcodeEnum(@http://www.w3.org/2003/05/soap-envelope).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.FaultcodeEnum.
 */
public class FaultcodeEnumImpl extends org.apache.xmlbeans.impl.values.JavaQNameHolderEx implements arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.FaultcodeEnum
{
    private static final long serialVersionUID = 1L;
    
    public FaultcodeEnumImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected FaultcodeEnumImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
