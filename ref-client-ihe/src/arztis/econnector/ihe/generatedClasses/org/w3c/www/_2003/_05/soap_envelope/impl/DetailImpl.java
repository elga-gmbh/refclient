/*
 * XML Type:  detail
 * Namespace: http://www.w3.org/2003/05/soap-envelope
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.Detail
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.impl;
/**
 * An XML detail(@http://www.w3.org/2003/05/soap-envelope).
 *
 * This is a complex type.
 */
public class DetailImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.Detail
{
    private static final long serialVersionUID = 1L;
    
    public DetailImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
