/*
 * XML Type:  Header
 * Namespace: http://www.w3.org/2003/05/soap-envelope
 * Java type: arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.Header
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.impl;
/**
 * An XML Header(@http://www.w3.org/2003/05/soap-envelope).
 *
 * This is a complex type.
 */
public class HeaderImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements arztis.econnector.ihe.generatedClasses.org.w3c.www._2003._05.soap_envelope.Header
{
    private static final long serialVersionUID = 1L;
    
    public HeaderImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
