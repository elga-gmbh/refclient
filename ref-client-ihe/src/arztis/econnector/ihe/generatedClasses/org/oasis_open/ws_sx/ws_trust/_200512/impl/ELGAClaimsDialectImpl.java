/*
 * XML Type:  ELGAClaimsDialect
 * Namespace: http://docs.oasis-open.org/ws-sx/ws-trust/200512
 * Java type: arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAClaimsDialect
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.impl;
/**
 * An XML ELGAClaimsDialect(@http://docs.oasis-open.org/ws-sx/ws-trust/200512).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAClaimsDialect.
 */
public class ELGAClaimsDialectImpl extends org.apache.xmlbeans.impl.values.JavaUriHolderEx implements arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAClaimsDialect
{
    private static final long serialVersionUID = 1L;
    
    public ELGAClaimsDialectImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected ELGAClaimsDialectImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
