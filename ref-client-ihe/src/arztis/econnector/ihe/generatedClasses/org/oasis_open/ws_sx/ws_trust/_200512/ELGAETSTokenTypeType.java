/*
 * XML Type:  ELGAETSTokenTypeType
 * Namespace: http://docs.oasis-open.org/ws-sx/ws-trust/200512
 * Java type: arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512;

/**
 * An XML ELGAETSTokenTypeType(@http://docs.oasis-open.org/ws-sx/ws-trust/200512).
 *
 * This is an atomic type that is a restriction of arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType.
 */
public interface ELGAETSTokenTypeType extends org.apache.xmlbeans.XmlString
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ELGAETSTokenTypeType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.s7BF52E4086C44A1A966935CB5C33C75C").resolveHandle("elgaetstokentypetype3e14type");
    
    org.apache.xmlbeans.StringEnumAbstractBase enumValue();
    void set(org.apache.xmlbeans.StringEnumAbstractBase e);
    
    static final Enum URN_ELGA_BES_2013_HCP_ASSERTION = Enum.forString("urn:elga:bes:2013:HCP:assertion");
    static final Enum URN_ELGA_BES_2013_SERVICE_ASSERTION = Enum.forString("urn:elga:bes:2013:service:assertion");
    static final Enum URN_ELGA_BES_2013_USER_ASSERTION_1 = Enum.forString("urn:elga:bes:2013:user:assertion:1");
    static final Enum URN_ELGA_BES_2013_MANDATE_ASSERTION_1 = Enum.forString("urn:elga:bes:2013:mandate:assertion:1");
    static final Enum URN_ELGA_BES_2013_MANDATE_ASSERTION_WIST = Enum.forString("urn:elga:bes:2013:mandate:assertion:WIST");
    static final Enum URN_ELGA_BES_2013_MANDATE_ASSERTION_OBST_1 = Enum.forString("urn:elga:bes:2013:mandate:assertion:OBST:1");
    static final Enum URN_ELGA_BES_2013_WIST_ASSERTION = Enum.forString("urn:elga:bes:2013:WIST:assertion");
    static final Enum URN_ELGA_BES_2019_CONTEXT_ASSERTION = Enum.forString("urn:elga:bes:2019:Context:assertion");
    static final Enum URN_ELGA_BES_2013_IDA_GDA_ASSERTION = Enum.forString("urn:elga:bes:2013:IDAgda:assertion");
    static final Enum URN_ELGA_KBS_CONTACT = Enum.forString("urn:elga:kbs:contact");
    static final Enum URN_ELGA_KBS_CONTACT_ECARD = Enum.forString("urn:elga:kbs:contact:ecard");
    static final Enum URN_ELGA_KBS_CONTACT_DELEGATE = Enum.forString("urn:elga:kbs:contact:delegate");
    static final Enum URN_ELGA_KBS_CONTACT_LIST = Enum.forString("urn:elga:kbs:contact:gda:list");
    static final Enum URN_ELGA_PAP_QUERY = Enum.forString("urn:elga:pap:query");
    static final Enum URN_ELGA_PAP_CREATE = Enum.forString("urn:elga:pap:create");
    static final Enum URN_ELGA_PAP_SUBMIT = Enum.forString("urn:elga:pap:submit");
    static final Enum URN_ELGA_2014_EMEDID_ASSERTION = Enum.forString("urn:elga:2014:EMEDID:assertion");
    
    static final int INT_URN_ELGA_BES_2013_HCP_ASSERTION = Enum.INT_URN_ELGA_BES_2013_HCP_ASSERTION;
    static final int INT_URN_ELGA_BES_2013_SERVICE_ASSERTION = Enum.INT_URN_ELGA_BES_2013_SERVICE_ASSERTION;
    static final int INT_URN_ELGA_BES_2013_USER_ASSERTION_1 = Enum.INT_URN_ELGA_BES_2013_USER_ASSERTION_1;
    static final int INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_1 = Enum.INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_1;
    static final int INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_WIST = Enum.INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_WIST;
    static final int INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_OBST_1 = Enum.INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_OBST_1;
    static final int INT_URN_ELGA_BES_2013_WIST_ASSERTION = Enum.INT_URN_ELGA_BES_2013_WIST_ASSERTION;
    static final int INT_URN_ELGA_BES_2019_CONTEXT_ASSERTION = Enum.INT_URN_ELGA_BES_2019_CONTEXT_ASSERTION;
    static final int INT_URN_ELGA_BES_2013_IDA_GDA_ASSERTION = Enum.INT_URN_ELGA_BES_2013_IDA_GDA_ASSERTION;
    static final int INT_URN_ELGA_KBS_CONTACT = Enum.INT_URN_ELGA_KBS_CONTACT;
    static final int INT_URN_ELGA_KBS_CONTACT_ECARD = Enum.INT_URN_ELGA_KBS_CONTACT_ECARD;
    static final int INT_URN_ELGA_KBS_CONTACT_DELEGATE = Enum.INT_URN_ELGA_KBS_CONTACT_DELEGATE;
    static final int INT_URN_ELGA_KBS_CONTACT_LIST = Enum.INT_URN_ELGA_KBS_CONTACT_LIST;
    static final int INT_URN_ELGA_PAP_QUERY = Enum.INT_URN_ELGA_PAP_QUERY;
    static final int INT_URN_ELGA_PAP_CREATE = Enum.INT_URN_ELGA_PAP_CREATE;
    static final int INT_URN_ELGA_PAP_SUBMIT = Enum.INT_URN_ELGA_PAP_SUBMIT;
    static final int INT_URN_ELGA_2014_EMEDID_ASSERTION = Enum.INT_URN_ELGA_2014_EMEDID_ASSERTION;
    
    /**
     * Enumeration value class for arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType.
     * These enum values can be used as follows:
     * <pre>
     * enum.toString(); // returns the string value of the enum
     * enum.intValue(); // returns an int value, useful for switches
     * // e.g., case Enum.INT_URN_ELGA_BES_2013_HCP_ASSERTION
     * Enum.forString(s); // returns the enum value for a string
     * Enum.forInt(i); // returns the enum value for an int
     * </pre>
     * Enumeration objects are immutable singleton objects that
     * can be compared using == object equality. They have no
     * public constructor. See the constants defined within this
     * class for all the valid values.
     */
    static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
    {
        /**
         * Returns the enum value for a string, or null if none.
         */
        public static Enum forString(java.lang.String s)
            { return (Enum)table.forString(s); }
        /**
         * Returns the enum value corresponding to an int, or null if none.
         */
        public static Enum forInt(int i)
            { return (Enum)table.forInt(i); }
        
        private Enum(java.lang.String s, int i)
            { super(s, i); }
        
        static final int INT_URN_ELGA_BES_2013_HCP_ASSERTION = 1;
        static final int INT_URN_ELGA_BES_2013_SERVICE_ASSERTION = 2;
        static final int INT_URN_ELGA_BES_2013_USER_ASSERTION_1 = 3;
        static final int INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_1 = 4;
        static final int INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_WIST = 5;
        static final int INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_OBST_1 = 6;
        static final int INT_URN_ELGA_BES_2013_WIST_ASSERTION = 7;
        static final int INT_URN_ELGA_KBS_CONTACT = 8;
        static final int INT_URN_ELGA_KBS_CONTACT_ECARD = 9;
        static final int INT_URN_ELGA_KBS_CONTACT_DELEGATE = 10;
        static final int INT_URN_ELGA_KBS_CONTACT_LIST = 11;
        static final int INT_URN_ELGA_PAP_QUERY = 12;
        static final int INT_URN_ELGA_PAP_CREATE = 13;
        static final int INT_URN_ELGA_PAP_SUBMIT = 14;
        static final int INT_URN_ELGA_2014_EMEDID_ASSERTION = 15;
        static final int INT_URN_ELGA_BES_2019_CONTEXT_ASSERTION = 16;
        static final int INT_URN_ELGA_BES_2013_IDA_GDA_ASSERTION = 17;
        
        public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
            new org.apache.xmlbeans.StringEnumAbstractBase.Table
        (
            new Enum[]
            {
                new Enum("urn:elga:bes:2013:HCP:assertion", INT_URN_ELGA_BES_2013_HCP_ASSERTION),
                new Enum("urn:elga:bes:2013:service:assertion", INT_URN_ELGA_BES_2013_SERVICE_ASSERTION),
                new Enum("urn:elga:bes:2013:user:assertion:1", INT_URN_ELGA_BES_2013_USER_ASSERTION_1),
                new Enum("urn:elga:bes:2013:mandate:assertion:1", INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_1),
                new Enum("urn:elga:bes:2013:mandate:assertion:WIST", INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_WIST),
                new Enum("urn:elga:bes:2013:mandate:assertion:OBST:1", INT_URN_ELGA_BES_2013_MANDATE_ASSERTION_OBST_1),
                new Enum("urn:elga:bes:2013:WIST:assertion", INT_URN_ELGA_BES_2013_WIST_ASSERTION),
                new Enum("urn:elga:kbs:contact", INT_URN_ELGA_KBS_CONTACT),
                new Enum("urn:elga:kbs:contact:ecard", INT_URN_ELGA_KBS_CONTACT_ECARD),
                new Enum("urn:elga:kbs:contact:delegate", INT_URN_ELGA_KBS_CONTACT_DELEGATE),
                new Enum("urn:elga:kbs:contact:gda:list", INT_URN_ELGA_KBS_CONTACT_LIST),
                new Enum("urn:elga:pap:query", INT_URN_ELGA_PAP_QUERY),
                new Enum("urn:elga:pap:create", INT_URN_ELGA_PAP_CREATE),
                new Enum("urn:elga:pap:submit", INT_URN_ELGA_PAP_SUBMIT),
                new Enum("urn:elga:2014:EMEDID:assertion", INT_URN_ELGA_2014_EMEDID_ASSERTION),
                new Enum("urn:elga:bes:2019:Context:assertion", INT_URN_ELGA_BES_2019_CONTEXT_ASSERTION),
                new Enum("urn:elga:bes:2013:IDAgda:assertion", INT_URN_ELGA_BES_2013_IDA_GDA_ASSERTION)
            }
        );
        private static final long serialVersionUID = 1L;
        private java.lang.Object readResolve() { return forInt(intValue()); } 
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType newValue(java.lang.Object obj) {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) type.newValue( obj ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType newInstance() {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.org.oasis_open.ws_sx.ws_trust._200512.ELGAETSTokenTypeType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
