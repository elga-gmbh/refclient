/**
 * Contains automated generated classes of Axis2 library for ELGA services like ETS, KBS, GDA-I, e-Medikation and XDS.
 */
package arztis.econnector.ihe.generatedClasses;