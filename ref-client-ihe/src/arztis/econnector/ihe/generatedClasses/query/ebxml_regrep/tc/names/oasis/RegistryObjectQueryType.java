/*
 * XML Type:  RegistryObjectQueryType
 * Namespace: urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0
 * Java type: arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType
 *
 * Automatically generated - do not modify.
 */
package arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis;


/**
 * An XML RegistryObjectQueryType(@urn:oasis:names:tc:ebxml-regrep:xsd:query:3.0).
 *
 * This is a complex type.
 */
public interface RegistryObjectQueryType extends arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.FilterQueryType
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(RegistryObjectQueryType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sA176CE12641784571DF8C98A0DCB19FE").resolveHandle("registryobjectquerytypeb1b2type");
    
    /**
     * Gets array of all "SlotBranch" elements
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.SlotBranchType[] getSlotBranchArray();
    
    /**
     * Gets ith "SlotBranch" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.SlotBranchType getSlotBranchArray(int i);
    
    /**
     * Returns number of "SlotBranch" element
     */
    int sizeOfSlotBranchArray();
    
    /**
     * Sets array of all "SlotBranch" element
     */
    void setSlotBranchArray(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.SlotBranchType[] slotBranchArray);
    
    /**
     * Sets ith "SlotBranch" element
     */
    void setSlotBranchArray(int i, arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.SlotBranchType slotBranch);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "SlotBranch" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.SlotBranchType insertNewSlotBranch(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "SlotBranch" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.SlotBranchType addNewSlotBranch();
    
    /**
     * Removes the ith "SlotBranch" element
     */
    void removeSlotBranch(int i);
    
    /**
     * Gets the "NameBranch" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.InternationalStringBranchType getNameBranch();
    
    /**
     * True if has "NameBranch" element
     */
    boolean isSetNameBranch();
    
    /**
     * Sets the "NameBranch" element
     */
    void setNameBranch(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.InternationalStringBranchType nameBranch);
    
    /**
     * Appends and returns a new empty "NameBranch" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.InternationalStringBranchType addNewNameBranch();
    
    /**
     * Unsets the "NameBranch" element
     */
    void unsetNameBranch();
    
    /**
     * Gets the "DescriptionBranch" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.InternationalStringBranchType getDescriptionBranch();
    
    /**
     * True if has "DescriptionBranch" element
     */
    boolean isSetDescriptionBranch();
    
    /**
     * Sets the "DescriptionBranch" element
     */
    void setDescriptionBranch(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.InternationalStringBranchType descriptionBranch);
    
    /**
     * Appends and returns a new empty "DescriptionBranch" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.InternationalStringBranchType addNewDescriptionBranch();
    
    /**
     * Unsets the "DescriptionBranch" element
     */
    void unsetDescriptionBranch();
    
    /**
     * Gets the "VersionInfoFilter" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.FilterType getVersionInfoFilter();
    
    /**
     * True if has "VersionInfoFilter" element
     */
    boolean isSetVersionInfoFilter();
    
    /**
     * Sets the "VersionInfoFilter" element
     */
    void setVersionInfoFilter(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.FilterType versionInfoFilter);
    
    /**
     * Appends and returns a new empty "VersionInfoFilter" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.FilterType addNewVersionInfoFilter();
    
    /**
     * Unsets the "VersionInfoFilter" element
     */
    void unsetVersionInfoFilter();
    
    /**
     * Gets array of all "ClassificationQuery" elements
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationQueryType[] getClassificationQueryArray();
    
    /**
     * Gets ith "ClassificationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationQueryType getClassificationQueryArray(int i);
    
    /**
     * Returns number of "ClassificationQuery" element
     */
    int sizeOfClassificationQueryArray();
    
    /**
     * Sets array of all "ClassificationQuery" element
     */
    void setClassificationQueryArray(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationQueryType[] classificationQueryArray);
    
    /**
     * Sets ith "ClassificationQuery" element
     */
    void setClassificationQueryArray(int i, arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationQueryType classificationQuery);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ClassificationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationQueryType insertNewClassificationQuery(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ClassificationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationQueryType addNewClassificationQuery();
    
    /**
     * Removes the ith "ClassificationQuery" element
     */
    void removeClassificationQuery(int i);
    
    /**
     * Gets array of all "ExternalIdentifierQuery" elements
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ExternalIdentifierQueryType[] getExternalIdentifierQueryArray();
    
    /**
     * Gets ith "ExternalIdentifierQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ExternalIdentifierQueryType getExternalIdentifierQueryArray(int i);
    
    /**
     * Returns number of "ExternalIdentifierQuery" element
     */
    int sizeOfExternalIdentifierQueryArray();
    
    /**
     * Sets array of all "ExternalIdentifierQuery" element
     */
    void setExternalIdentifierQueryArray(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ExternalIdentifierQueryType[] externalIdentifierQueryArray);
    
    /**
     * Sets ith "ExternalIdentifierQuery" element
     */
    void setExternalIdentifierQueryArray(int i, arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ExternalIdentifierQueryType externalIdentifierQuery);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "ExternalIdentifierQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ExternalIdentifierQueryType insertNewExternalIdentifierQuery(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "ExternalIdentifierQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ExternalIdentifierQueryType addNewExternalIdentifierQuery();
    
    /**
     * Removes the ith "ExternalIdentifierQuery" element
     */
    void removeExternalIdentifierQuery(int i);
    
    /**
     * Gets the "ObjectTypeQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationNodeQueryType getObjectTypeQuery();
    
    /**
     * True if has "ObjectTypeQuery" element
     */
    boolean isSetObjectTypeQuery();
    
    /**
     * Sets the "ObjectTypeQuery" element
     */
    void setObjectTypeQuery(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationNodeQueryType objectTypeQuery);
    
    /**
     * Appends and returns a new empty "ObjectTypeQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationNodeQueryType addNewObjectTypeQuery();
    
    /**
     * Unsets the "ObjectTypeQuery" element
     */
    void unsetObjectTypeQuery();
    
    /**
     * Gets the "StatusQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationNodeQueryType getStatusQuery();
    
    /**
     * True if has "StatusQuery" element
     */
    boolean isSetStatusQuery();
    
    /**
     * Sets the "StatusQuery" element
     */
    void setStatusQuery(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationNodeQueryType statusQuery);
    
    /**
     * Appends and returns a new empty "StatusQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.ClassificationNodeQueryType addNewStatusQuery();
    
    /**
     * Unsets the "StatusQuery" element
     */
    void unsetStatusQuery();
    
    /**
     * Gets array of all "SourceAssociationQuery" elements
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType[] getSourceAssociationQueryArray();
    
    /**
     * Gets ith "SourceAssociationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType getSourceAssociationQueryArray(int i);
    
    /**
     * Returns number of "SourceAssociationQuery" element
     */
    int sizeOfSourceAssociationQueryArray();
    
    /**
     * Sets array of all "SourceAssociationQuery" element
     */
    void setSourceAssociationQueryArray(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType[] sourceAssociationQueryArray);
    
    /**
     * Sets ith "SourceAssociationQuery" element
     */
    void setSourceAssociationQueryArray(int i, arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType sourceAssociationQuery);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "SourceAssociationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType insertNewSourceAssociationQuery(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "SourceAssociationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType addNewSourceAssociationQuery();
    
    /**
     * Removes the ith "SourceAssociationQuery" element
     */
    void removeSourceAssociationQuery(int i);
    
    /**
     * Gets array of all "TargetAssociationQuery" elements
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType[] getTargetAssociationQueryArray();
    
    /**
     * Gets ith "TargetAssociationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType getTargetAssociationQueryArray(int i);
    
    /**
     * Returns number of "TargetAssociationQuery" element
     */
    int sizeOfTargetAssociationQueryArray();
    
    /**
     * Sets array of all "TargetAssociationQuery" element
     */
    void setTargetAssociationQueryArray(arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType[] targetAssociationQueryArray);
    
    /**
     * Sets ith "TargetAssociationQuery" element
     */
    void setTargetAssociationQueryArray(int i, arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType targetAssociationQuery);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "TargetAssociationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType insertNewTargetAssociationQuery(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "TargetAssociationQuery" element
     */
    arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.AssociationQueryType addNewTargetAssociationQuery();
    
    /**
     * Removes the ith "TargetAssociationQuery" element
     */
    void removeTargetAssociationQuery(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType newInstance() {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (arztis.econnector.ihe.generatedClasses.query.ebxml_regrep.tc.names.oasis.RegistryObjectQueryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
