/**
 * Provides functionalities for ELGA SOAP requests, which are partly based on IHE.
 */
package arztis.econnector.ihe;