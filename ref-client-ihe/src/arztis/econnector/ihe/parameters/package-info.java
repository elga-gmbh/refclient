/**
 * Provides classes which are needed to pass data to ELGA requests.  
 */
package arztis.econnector.ihe.parameters;