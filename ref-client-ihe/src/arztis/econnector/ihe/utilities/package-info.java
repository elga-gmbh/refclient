/**
 * Provides classes with utilities for IHE purposes like CDA generation, HL7
 * messages and so on.
 */
package arztis.econnector.ihe.utilities;