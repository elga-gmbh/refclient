/**
 * Provides common classes which are needed to build and send more than one ELGA request
 */
package arztis.econnector.ihe.queries.common;