/**
 * Provides classes which are needed to handle authentication details for ELGA requests. 
 */
package arztis.econnector.ihe.model;