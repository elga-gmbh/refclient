/**
 * 
 * Contributors:
 *   ET-Innovations GmbH - initial API and implementation
 * 
 */
package arztis.econnector.ihe.model;

import org.husky.communication.at.ConvenienceCommunicationAt;
import org.springframework.stereotype.Component;


@Component
public class ConvenienceCommunicationConnection extends ConvenienceCommunicationAt {

	public ConvenienceCommunicationConnection() {
		super();
	}

}
