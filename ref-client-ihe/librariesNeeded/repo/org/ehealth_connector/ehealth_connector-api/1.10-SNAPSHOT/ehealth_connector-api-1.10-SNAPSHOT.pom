<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.ehealth_connector</groupId>
	<artifactId>ehealth_connector-api</artifactId>
	<version>1.10-SNAPSHOT</version>
	<url>https://gitlab.com/ehealth-connector/api/wikis/Home</url>
	<name>eHealth Connector main Git project Api</name>
	<packaging>pom</packaging>
	<description>The eHealthConnector (eHC) is a convenience API for IHE transactions and CDA documents. It supports functionalities for cross-enterprise sharing of documents and master patient index (MPI) connection using IHE transactions and also supports CDA content profiles. It allows easy access and conformity to a local eHealth trust space, such as for example Switzerland or Ausria.	The eHC is built on the basis of the OHT IHE profiles.</description>

	<!-- <parent> <groupId>healthcare.suisse-open-exchange.maven</groupId> <artifactId>suisse-open-exchange-top-pom</artifactId> 
		<version>1.0.1</version> </parent> -->

	<scm>
		<connection>scm:git:https://gitlab.com/ehealth-connector/api.git</connection>
		<developerConnection>scm:git:git@gitlab.com:ehealth-connector/api.git</developerConnection>
		<url>https://gitlab.com/ehealth-connector/api/tree/master</url>
		<tag>HEAD</tag>
	</scm>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.build.timestamp.format>yyyy-MM-dd HH:mm</maven.build.timestamp.format>
		<java.version>12</java.version>

		<oht.version>2.4-SNAPSHOT</oht.version>
		<mdht.version>2.5.0</mdht.version>
		<hapi.fhir.version>5.0.2</hapi.fhir.version>
		<eclipse.emf.version>2.6.0.v20100614-1136</eclipse.emf.version>
		<eclipse.ocl.version>1.3.0.v200905271400</eclipse.ocl.version>
		<log4j2.version>2.13.3</log4j2.version>
		<axis2.version>1.7.9</axis2.version>
		<opensaml.version>3.4.5</opensaml.version>
		<org.apache.ws.commons.axiom.version>1.2.21</org.apache.ws.commons.axiom.version>

	</properties>

	<repositories>
		<repository>
			<id>projects.suisse-open-exchange.healthcare-sna</id>
			<name>projects.suisse-open-exchange.healthcare-snapshots</name>
			<url>https://projects.suisse-open-exchange.healthcare/artifactory/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
		</repository>
		<repository>
			<id>projects.suisse-open-exchange.healthcare-rel</id>
			<name>projects.suisse-open-exchange.healthcare-releases</name>
			<url>https://projects.suisse-open-exchange.healthcare/artifactory/releases</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>mvnrepository.com</id>
			<name>MVN Repository</name>
			<url>http://mvnrepository.com/artifact/</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>Shibboleth repo</id>
			<url>https://build.shibboleth.net/nexus/content/repositories/releases</url>
		</repository>
	</repositories>
	<pluginRepositories>
		<pluginRepository>
			<id>suisse-open-exchange-repository</id>
			<name>Suisse Open Exchange Maven Repository</name>
			<url>https://projects.suisse-open-exchange.healthcare/artifactory/releases</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>




	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.openhealthtools.mdht.uml</groupId>
				<artifactId>org.openhealthtools.mdht.uml.cda</artifactId>
				<version>${mdht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.mdht.uml.hl7</groupId>
				<artifactId>org.openhealthtools.mdht.uml.hl7.datatypes</artifactId>
				<version>${mdht.version}</version>
			</dependency>


			<!-- open health tools ihe utils -->
			<dependency>
				<groupId>org.openhealthtools.ihe.utils</groupId>
				<artifactId>oht-ihe-utils</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ihe ebXML -->
			<dependency>
				<groupId>org.openhealthtools.ihe.common.ebXML</groupId>
				<artifactId>oht-common-ebXML._2._1</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.common.ebXML</groupId>
				<artifactId>oht-common-ebXML._3._0</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ihe hl7v2 -->
			<dependency>
				<groupId>org.openhealthtools.ihe.common.hl7v2</groupId>
				<artifactId>oht-common-hl7v2</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.common.hl7v2</groupId>
				<artifactId>oht-common-hl7v2-client</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ihe hl7v3 -->
			<dependency>
				<groupId>org.openhealthtools.ihe.common.hl7v3</groupId>
				<artifactId>oht-common-hl7v3</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.common.hl7v3</groupId>
				<artifactId>oht-common-hl7v3-client</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.common.hl7v3</groupId>
				<artifactId>oht-common-hl7v3-soap</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ihe atna -->
			<dependency>
				<groupId>org.openhealthtools.ihe.atna</groupId>
				<artifactId>oht-atna-auditor</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.atna</groupId>
				<artifactId>oht-atna-context</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.atna</groupId>
				<artifactId>oht-atna-nodeauth</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ihe xds -->
			<dependency>
				<groupId>org.openhealthtools.ihe.xds</groupId>
				<artifactId>oht-xds</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.xds</groupId>
				<artifactId>oht-xds-metadata</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.xds.metadata</groupId>
				<artifactId>oht-xds-metadata-transform</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.xds.metadata</groupId>
				<artifactId>oht-xds-metadata-extract</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.xds.metadata</groupId>
				<artifactId>oht-xds-metadata-extract-cdar2</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.xds</groupId>
				<artifactId>oht-xds-source</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.xds</groupId>
				<artifactId>oht-xds-consumer</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.xds</groupId>
				<artifactId>oht-xds-soap</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<dependency>
				<groupId>org.openhealthtools.ihe</groupId>
				<artifactId>oht-xdm</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<dependency>
				<groupId>org.openhealthtools.ihe.common.ws</groupId>
				<artifactId>oht-common-ws-axis2</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ebxml -->
			<dependency>
				<groupId>org.openhealthtools.ihe.common</groupId>
				<artifactId>oht-common-ebXML._2._1</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.common</groupId>
				<artifactId>oht-common-ebXML._3._0</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ihe pix -->
			<dependency>
				<groupId>org.openhealthtools.ihe.pix</groupId>
				<artifactId>oht-pix-consumer-v3</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.pix</groupId>
				<artifactId>oht-pix-consumer</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.pix</groupId>
				<artifactId>oht-pix-source</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.pix</groupId>
				<artifactId>oht-pix-source-v3</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<dependency>
				<groupId>org.openhealthtools.ihe.pdq</groupId>
				<artifactId>oht-pdq-consumer</artifactId>
				<version>${oht.version}</version>
			</dependency>
			<dependency>
				<groupId>org.openhealthtools.ihe.pdq</groupId>
				<artifactId>oht-pdq-consumer-v3</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- open health tools ihe xua -->
			<dependency>
				<groupId>org.openhealthtools.ihe.xua</groupId>
				<artifactId>oht-xua</artifactId>
				<version>${oht.version}</version>
			</dependency>

			<!-- hl7 -->
			<dependency>
				<groupId>org.hl7.v3</groupId>
				<artifactId>hl7v3-core</artifactId>
				<version>1.0.0</version>
			</dependency>

			<dependency>
				<groupId>org.eclipse.ohf</groupId>
				<artifactId>org.eclipse.ohf.utilities</artifactId>
				<version>0.2.0.200812311053</version>
			</dependency>


			<!-- hapi fhir -->
			<dependency>
				<groupId>ca.uhn.hapi.fhir</groupId>
				<artifactId>hapi-fhir-structures-dstu3</artifactId>
				<version>${hapi.fhir.version}</version>
				<exclusions>
					<exclusion>
						<groupId>commons-httpclient</groupId>
						<artifactId>commons-httpclient</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
			<dependency>
				<groupId>ca.uhn.hapi.fhir</groupId>
				<artifactId>hapi-fhir-validation-resources-dstu3</artifactId>
				<version>${hapi.fhir.version}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.httpcomponents</groupId>
				<artifactId>httpmime</artifactId>
				<version>4.4.1</version>
			</dependency>
			<dependency>
				<groupId>org.apache.httpcomponents</groupId>
				<version>4.4.1</version>
				<artifactId>httpclient</artifactId>
			</dependency>
			<dependency>
				<groupId>org.apache.httpcomponents</groupId>
				<version>4.4.1</version>
				<artifactId>httpcore</artifactId>
			</dependency>
			<dependency>
				<groupId>org.apache.httpcomponents</groupId>
				<version>4.4.1</version>
				<artifactId>httpcore-nio</artifactId>
			</dependency>

			<!-- XUA stuff -->
			<dependency>
				<groupId>xerces</groupId>
				<artifactId>xercesImpl</artifactId>
				<version>2.12.0</version>
				<exclusions>
					<exclusion>
						<groupId>xml-apis</groupId>
						<artifactId>xml-apis</artifactId>
					</exclusion>
				</exclusions>
			</dependency>

			<dependency>
				<groupId>org.bouncycastle</groupId>
				<artifactId>bcprov-jdk15on</artifactId>
				<version>1.66</version>
			</dependency>

			<dependency>
				<groupId>com.google.guava</groupId>
				<artifactId>guava</artifactId>
				<version>29.0-jre</version>
			</dependency>

			<dependency>
				<groupId>org.apache.santuario</groupId>
				<artifactId>xmlsec</artifactId>
				<version>2.2.0</version>
			</dependency>

			<dependency>
				<groupId>org.opensaml</groupId>
				<artifactId>opensaml-saml-impl</artifactId>
				<version>${opensaml.version}</version>
				<exclusions>
					<exclusion>
						<groupId>commons-collections</groupId>
						<artifactId>commons-collections</artifactId>
					</exclusion>
					<exclusion>
						<groupId>com.google.guava</groupId>
						<artifactId>guava</artifactId>
					</exclusion>
					<exclusion>
						<groupId>org.bouncycastle</groupId>
						<artifactId>bcprov-jdk15on</artifactId>
					</exclusion>
				</exclusions>
			</dependency>

			<dependency>
				<groupId>org.jsoup</groupId>
				<artifactId>jsoup</artifactId>
				<version>1.11.2</version>
			</dependency>

			<!-- axis2 stuff -->
			<dependency>
				<groupId>org.apache.axis2</groupId>
				<artifactId>axis2-kernel</artifactId>
				<version>${axis2.version}</version>
				<exclusions>
					<exclusion>
						<groupId>xml-apis</groupId>
						<artifactId>xml-apis</artifactId>
					</exclusion>
					<exclusion>
						<groupId>org.apache.ws.commons.axiom</groupId>
						<artifactId>axiom-api</artifactId>
					</exclusion>
				</exclusions>
			</dependency>

			<dependency>
				<groupId>org.apache.axis2</groupId>
				<artifactId>axis2-java2wsdl</artifactId>
				<version>${axis2.version}</version>
				<exclusions>
					<exclusion>
						<groupId>xml-apis</groupId>
						<artifactId>xml-apis</artifactId>
					</exclusion>
					<exclusion>
						<groupId>org.apache.ws.commons.axiom</groupId>
						<artifactId>axiom-api</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
			<dependency>
				<groupId>org.apache.axis2</groupId>
				<artifactId>axis2-saaj</artifactId>
				<version>${axis2.version}</version>
				<exclusions>
					<exclusion>
						<groupId>xml-apis</groupId>
						<artifactId>xml-apis</artifactId>
					</exclusion>
					<exclusion>
						<groupId>org.apache.ws.commons.axiom</groupId>
						<artifactId>axiom-api</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
			<dependency>
				<groupId>org.apache.axis2</groupId>
				<artifactId>axis2-adb-codegen</artifactId>
				<version>${axis2.version}</version>
				<exclusions>
					<exclusion>
						<groupId>xml-apis</groupId>
						<artifactId>xml-apis</artifactId>
					</exclusion>
					<exclusion>
						<groupId>org.apache.ws.commons.axiom</groupId>
						<artifactId>axiom-api</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
			<dependency>
				<groupId>org.apache.ws.commons.axiom</groupId>
				<artifactId>axiom-impl</artifactId>
				<version>${org.apache.ws.commons.axiom.version}</version>
				<exclusions>
					<exclusion>
						<groupId>commons-logging</groupId>
						<artifactId>commons-logging</artifactId>
					</exclusion>
					<exclusion>
						<groupId>jaxen</groupId>
						<artifactId>jaxen</artifactId>
					</exclusion>
					<exclusion>
						<groupId>org.apache.geronimo.specs</groupId>
						<artifactId>geronimo-stax-api_1.0_spec</artifactId>
					</exclusion>
					<exclusion>
						<groupId>org.codehaus.woodstox</groupId>
						<artifactId>stax2-api</artifactId>
					</exclusion>
				</exclusions>
			</dependency>
			<dependency>
				<groupId>org.apache.axis2</groupId>
				<artifactId>addressing</artifactId>
				<version>${axis2.version}</version>
				<classifier>classpath-module</classifier>
			</dependency>
			<dependency>
				<groupId>commons-logging</groupId>
				<artifactId>commons-logging</artifactId>
				<version>1.1.1</version>
			</dependency>
			<dependency>
				<groupId>commons-lang</groupId>
				<artifactId>commons-lang</artifactId>
				<version>2.4</version>
			</dependency>
			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-lang3</artifactId>
				<version>3.9</version>
			</dependency>
			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-text</artifactId>
				<version>1.7</version>
			</dependency>
			<dependency>
				<groupId>commons-cli</groupId>
				<artifactId>commons-cli</artifactId>
				<version>1.4</version>
			</dependency>
			<dependency>
				<groupId>commons-codec</groupId>
				<artifactId>commons-codec</artifactId>
				<version>1.10</version>
			</dependency>
			<dependency>
				<groupId>commons-io</groupId>
				<artifactId>commons-io</artifactId>
				<version>2.4</version>
			</dependency>

			<dependency>
				<groupId>commons-fileupload</groupId>
				<artifactId>commons-fileupload</artifactId>
				<version>1.4</version>
			</dependency>

			<!-- configuration stuff (e.g. for value set handling) -->
			<dependency>
				<groupId>com.jayway.jsonpath</groupId>
				<artifactId>json-path</artifactId>
				<version>2.4.0</version>
			</dependency>
			<dependency>
				<groupId>org.yaml</groupId>
				<artifactId>snakeyaml</artifactId>
				<version>1.24</version>
			</dependency>

			<!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core -->
			<dependency>
				<groupId>org.apache.logging.log4j</groupId>
				<artifactId>log4j-core</artifactId>
				<version>${log4j2.version}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.logging.log4j</groupId>
				<artifactId>log4j-slf4j-impl</artifactId>
				<version>${log4j2.version}</version>
			</dependency>

			<dependency>
				<groupId>javax.servlet</groupId>
				<artifactId>servlet-api</artifactId>
				<version>2.3</version>
				<scope>provided</scope>
			</dependency>

			<!-- validator stuff -->
			<dependency>
				<groupId>net.sf.saxon</groupId>
				<artifactId>Saxon-HE</artifactId>
				<version>9.9.0-1</version>
			</dependency>
			<dependency>
				<groupId>com.pdftools</groupId>
				<artifactId>VALA</artifactId>
				<version>5.9.4.9</version>
			</dependency>
			<dependency>
				<groupId>org.verapdf</groupId>
				<artifactId>validation-model</artifactId>
				<version>1.16.1</version>
			</dependency>

			<!-- JAXB binding stuff -->
			<dependency>
				<groupId>org.glassfish.jaxb</groupId>
				<artifactId>jaxb-runtime</artifactId>
				<version>3.0.0-M4</version>
			</dependency>
			<dependency>
				<groupId>com.sun.xml.bind</groupId>
				<artifactId>jaxb-impl</artifactId>
				<version>3.0.0-M4</version>
			</dependency>

			<dependency>
				<groupId>com.sun.xml.bind</groupId>
				<artifactId>jaxb-core</artifactId>
				<version>3.0.0-M4</version>
			</dependency>

			<dependency>
				<groupId>javax.xml.bind</groupId>
				<artifactId>jaxb-api</artifactId>
				<version>2.3.1</version>
			</dependency>

			<!-- test libs -->
			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>4.12</version>
			</dependency>

			<dependency>
				<groupId>org.opensaml</groupId>
				<artifactId>opensaml-xacml-saml-impl</artifactId>
				<version>${opensaml.version}</version>
			</dependency>
			<dependency>
				<groupId>org.opensaml</groupId>
				<artifactId>opensaml-xacml-impl</artifactId>
				<version>${opensaml.version}</version>
			</dependency>

		</dependencies>
	</dependencyManagement>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-dependency-plugin</artifactId>
					<version>2.10</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>2.5.1</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-site-plugin</artifactId>
					<version>3.7.1</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-release-plugin</artifactId>
					<version>2.5.3</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-assembly-plugin</artifactId>
					<version>2.6</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>2.6</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-antrun-plugin</artifactId>
					<version>1.8</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>2.10.3</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-project-info-reports-plugin</artifactId>
					<version>2.4</version>
				</plugin>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>build-helper-maven-plugin</artifactId>
					<version>1.10</version>
				</plugin>
				<plugin>
					<groupId>com.googlecode.maven-java-formatter-plugin</groupId>
					<artifactId>maven-java-formatter-plugin</artifactId>
					<version>0.4</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-eclipse-plugin</artifactId>
					<version>2.10</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<version>2.6</version>
				</plugin>
			</plugins>

		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<configuration>
					<encoding>UTF-8</encoding>
					<nonFilteredFileExtensions>
						<nonFilteredFileExtension>jks</nonFilteredFileExtension>
						<nonFilteredFileExtension>pdf</nonFilteredFileExtension>
						<nonFilteredFileExtension>zip</nonFilteredFileExtension>
					</nonFilteredFileExtensions>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<configuration>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>false</downloadJavadocs>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<forceJavacCompilerUse>true</forceJavacCompilerUse>
					<source>${java.version}</source>
					<target>${java.version}</target>
					<release>${java.version}</release>
					<encoding>${project.build.sourceEncoding}</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<useDefaultManifestFile>true</useDefaultManifestFile>
					<archive>
						<manifest>
							<addClasspath>true</addClasspath>
						</manifest>
						<manifestEntries>
							<Build-Time>${maven.build.timestamp}</Build-Time>
						</manifestEntries>
						<manifestSections>
							<manifestSection>
								<name>ProjektInfos</name>
								<manifestEntries>
									<Projekt>${project.name}</Projekt>
									<Version>${project.version}</Version>
									<GroupId>${project.groupId}</GroupId>
									<ArtifactId>${project.artifactId}</ArtifactId>
									<Url>https://sourceforge.net/projects/ehealthconnector/</Url>
								</manifestEntries>
							</manifestSection>
						</manifestSections>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<configuration>
					<additionalparam>-Xdoclint:none</additionalparam>
					<failOnError>false</failOnError>
					<stylesheetfile>src/site/apidocs/stylesheet.css</stylesheetfile>
					<aggregate>true</aggregate>
					<overview>${basedir}/src/site/apidocs/overview.html</overview>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<reporting>
		<plugins>
			<!-- <plugin> <groupId>org.codehaus.mojo</groupId> <artifactId>cobertura-maven-plugin</artifactId> 
				<version>2.7</version> <configuration> <formats> <format>xml</format> </formats> 
				</configuration> </plugin> -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>3.7.1</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.10.3</version>
				<configuration>
					<additionalparam>-Xdoclint:none</additionalparam>
					<failOnError>false</failOnError>
					<stylesheetfile>src/site/apidocs/stylesheet.css</stylesheetfile>
					<aggregate>true</aggregate>
					<overview>${basedir}/src/site/apidocs/overview.html</overview>
				</configuration>
			</plugin>
			<!-- <plugin> <groupId>org.apache.maven.plugins</groupId> <artifactId>maven-checkstyle-plugin</artifactId> 
				<version>2.17</version> </plugin> -->
		</plugins>
	</reporting>

	<issueManagement>
		<url>https://gitlab.com/groups/ehealth-connector/-/issues</url>
	</issueManagement>
	<organization>
		<name>eHealth Connector</name>
		<url>https://gitlab.com/ehealth-connector</url>
	</organization>

	<developers>
		<developer>
			<id>tschaller</id>
			<name>Tony Schaller</name>
			<organization>medshare GmbH</organization>
			<organizationUrl>https://medshare.net</organizationUrl>
			<url>http://sourceforge.net/u/tschaller/profile/</url>
			<roles>
				<role>Project Leader</role>
			</roles>
		</developer>
		<developer>
			<id>ahelmer</id>
			<name>Axel Helmer</name>
			<url>http://sourceforge.net/u/ahelmer/profile/</url>
			<roles>
				<role>Developer</role>
			</roles>
		</developer>
		<developer>
			<id>oegger</id>
			<name>Oliver Egger</name>
			<organization>ahdis GmbH</organization>
			<organizationUrl>http://www.ahdis.ch/</organizationUrl>
			<url>http://sourceforge.net/u/ahelmer/profile/</url>
			<roles>
				<role>Developer</role>
			</roles>
		</developer>
		<developer>
			<id>michaelonken</id>
			<name>Michael Onken</name>
			<url>http://sourceforge.net/u/michaelonken/profile/</url>
		</developer>
		<developer>
			<id>schmidco</id>
			<name>Schmid Cornelia</name>
			<url>http://sourceforge.net/u/schmidco/profile/</url>
		</developer>
		<developer>
			<id>rluykx</id>
			<name>Roeland Luykx</name>
			<url>http://sourceforge.net/u/rluykx/profile</url>
			<organization>Arpage AG</organization>
			<organizationUrl>https://www.arpage.ch</organizationUrl>
			<roles>
				<role>Maven Specialist</role>
				<role>Developer</role>
				<role>Reviewer</role>
			</roles>
		</developer>
	</developers>

	<distributionManagement>
		<repository>
			<id>projects.suisse-open-exchange.healthcare-rel</id>
			<name>projects.suisse-open-exchange.healthcare-releases</name>
			<url>https://projects.suisse-open-exchange.healthcare/artifactory/releases</url>
		</repository>
		<snapshotRepository>
			<id>projects.suisse-open-exchange.healthcare-sna</id>
			<name>projects.suisse-open-exchange.healthcare-snapshots</name>
			<url>https://projects.suisse-open-exchange.healthcare/artifactory/snapshots</url>
		</snapshotRepository>
	</distributionManagement>

	<ciManagement>
		<url>https://projects.suisse-open-exchange.healthcare/ci</url>
		<system>Jenkins</system>
	</ciManagement>



</project>
