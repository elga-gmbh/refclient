/**
 * Provides the classes which are needed for the JAVA and REST API of the ELGA reference client
 */
package arztis.econnector.rest;