/**
 * Provides classes which represents HL7 FHIR resources and CDA documents.
 */
package arztis.econnector.rest.model;