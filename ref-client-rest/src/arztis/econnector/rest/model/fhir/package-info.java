/**
 * Provides classes of different HL7 FHIR resources, which are needed for REST interface.
 */
package arztis.econnector.rest.model.fhir;