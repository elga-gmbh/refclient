/**
 * 
 * Contributors:
 *   ET-Innovations GmbH - initial API and implementation
 * 
 */
package arztis.econnector.rest.utilities;

/**
 * This class contains constants for JSON objects. It contains parameter names of
 * expected JSON objects.
 *
 * @author Anna Jungwirth
 *
 */
public class JsonUtilRest {

	/** The Constant JSON_PARAM_ROLE. */
	public static final String JSON_PARAM_ROLE = "role";
	
	/** The Constant JSON_PARAM_PIN. */
	public static final String JSON_PARAM_PIN = "password";
	
	/** The Constant JSON_PARAM_CARD_READER_ID. */
	public static final String JSON_PARAM_CARD_READER_ID = "cardReaderId";
	
	/** The Constant JSON_PARAM_ORGANIZATION_ID. */
	public static final String JSON_PARAM_ORGANIZATION_ID = "organizationid";
	
	/** The Constant JSON_PARAM_SUBJECT_ID. */
	public static final String JSON_PARAM_SUBJECT_ID = "subjectid";
	
	/** The Constant JSON_PARAM_TYPE. */
	public static final String JSON_PARAM_TYPE = "type";
	
	/** The Constant JSON_PARAM_IDENTIFIER. */
	public static final String JSON_PARAM_IDENTIFIER = "identifier";
	
	/** The Constant JSON_PARAM_PATIENT_IDENTIFIER. */
	public static final String JSON_PARAM_PATIENT_IDENTIFIER = "patient.identifier";
	
	/** The Constant JSON_PARAM_CONTENT_ATTACHMENT_CONTENT_TYPE. */
	public static final String JSON_PARAM_CONTENT_ATTACHMENT_CONTENT_TYPE = "content.attachment.contentType";
	
	/** The Constant JSON_PARAM_CATEGORY. */
	public static final String JSON_PARAM_CATEGORY = "category";
	
	/** The Constant JSON_PARAM_STATUS. */
	public static final String JSON_PARAM_STATUS = "status";
	
	/** The Constant JSON_PARAM_AUTHOR_ORGANIZATION_NAME. */
	public static final String JSON_PARAM_AUTHOR_ORGANIZATION_NAME = "author.organization.name";
	
	/** The Constant JSON_PARAM_ORGANIZATION. */
	public static final String JSON_PARAM_ORGANIZATION = "organization";
	
	/** The Constant JSON_PARAM_GIVENNAME. */
	public static final String JSON_PARAM_GIVENNAME = "given";
	
	/** The Constant JSON_PARAM_FAMILYNAME. */
	public static final String JSON_PARAM_FAMILYNAME = "family";
	
	/** The Constant JSON_PARAM_PREFIX. */
	public static final String JSON_PARAM_PREFIX = "prefix";
	
	/** The Constant JSON_PARAM_SUFFIX. */
	public static final String JSON_PARAM_SUFFIX = "suffix";
	
	/** The Constant JSON_PARAM_BIRTHDATE. */
	public static final String JSON_PARAM_BIRTHDATE = "birthdate";
	
	/** The Constant JSON_PARAM_GENDER. */
	public static final String JSON_PARAM_GENDER = "gender";
	
	/** The Constant JSON_PARAM_STREETADDRESSLINE. */
	public static final String JSON_PARAM_STREETADDRESSLINE = "address-line";
	
	/** The Constant JSON_PARAM_CITY. */
	public static final String JSON_PARAM_CITY = "address-city";
	
	/** The Constant JSON_PARAM_COUNTRY. */
	public static final String JSON_PARAM_COUNTRY = "address-country";
	
	/** The Constant JSON_PARAM_POSTALCODE. */
	public static final String JSON_PARAM_POSTALCODE = "address-postalcode";
	
	/** The Constant JSON_PARAM_STATE. */
	public static final String JSON_PARAM_STATE = "address-state";
	
	/** The Constant JSON_PARAM_ADDRESSTYPE. */
	public static final String JSON_PARAM_ADDRESSTYPE = "addresstype";
	
	/** The Constant JSON_PARAM_ACTIVE. */
	public static final String JSON_PARAM_ACTIVE = "active";

	/**
	 * Instantiates a new json util rest.
	 */
	private JsonUtilRest() {
		throw new IllegalStateException("Utility class");
	}

}
